using System;
using System.Net.Sockets;
using System.IO;
using System.ComponentModel;

namespace StasisCommandCenter
{
	/// <summary>
	/// Sends/Receives messages between the Stasis robot and 
	/// the command center.
	/// </summary>
	public class CommunicationLink
	{
		/// <summary>
		/// Raised when a message has been received over the communication link
		/// </summary>
		public event MessageReceivedEventHandler MessageReceived = delegate {};
		
		/// <summary>
		/// TCP Client used for communication
		/// </summary>
		private TcpClient tcpClient = null;
		
		/// <summary>
		/// Worker thread for reading stream data
		/// </summary>
		private BackgroundWorker worker = null;
		
		/// <summary>
		/// Constructor
		/// </summary>
		public CommunicationLink()
		{
			
		}
		
		/// <summary>
		/// Connect the specified ip and port.
		/// </summary>
		/// <param name='ip'>
		/// Ip Address to connect to
		/// </param>
		/// <param name='port'>
		/// Port on IP Address to connect to
		/// </param>
		public void Connect(string ip, int port)
		{
			tcpClient = new TcpClient();
			tcpClient.Connect(ip, port);
			
			// Start up worker thread that will
			// listen to messages from the stasis
			worker = new BackgroundWorker();
			worker.WorkerReportsProgress = true;
			worker.DoWork += HandleWorkerDoWork;
			worker.RunWorkerAsync();
		}
		
		/// <summary>
		/// Disconnect from the Stasis
		/// </summary>
		public void Disconnect()
		{
			tcpClient.Close();
		}
		
		/// <summary>
		/// Reads data from the tcp connection and looks for messages
		/// </summary>
		private void HandleWorkerDoWork (object sender, DoWorkEventArgs e)
		{
			byte[] buffer = new byte[128];
			byte bufferLength = 0;
			
			while(tcpClient.Connected)
			{
				if(tcpClient.Available > 0)
				{
					byte b = (byte)tcpClient.GetStream().ReadByte();
					if(bufferLength < 4)
					{
						// Still haven't received start bytes
						if(b == 0xFF)
						{
							// Received a start byte
							buffer[bufferLength++] = b;
						}
						else
						{
							// Start byte chain broken
							bufferLength = 0;
						}
					}
					else
					{
						// Got a data byte
						buffer[bufferLength++] = b;
						
						// Received end bytes?
						if(bufferLength > 8 && 
						   buffer[bufferLength - 1] == 0xFF && 
						   buffer[bufferLength - 2] == 0xFF && 
						   buffer[bufferLength - 3] == 0xFF && 
						   buffer[bufferLength - 4] == 0xFE)
						{
							// Got a message
							byte[] data = new byte[bufferLength - 9];
							Array.Copy(buffer, 5, data, 0, bufferLength - 1);
							MessageType type = (MessageType)buffer[4];
							
							this.MessageReceived(this, new MessageReceivedEventArgs(type, data));
						}
					}
				}
			}
		}
	}
}

