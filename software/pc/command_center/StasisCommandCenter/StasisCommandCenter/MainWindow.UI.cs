using System;
using System.Windows.Forms;
using System.Drawing;

namespace StasisCommandCenter
{
	public partial class MainWindow
	{
		private MainMenu mainMenu = new MainMenu();
		private FlowLayoutPanel leftPanel = new FlowLayoutPanel();
		private Panel middlePanel = new Panel();
		private SplitContainer rightSplitContainer = new SplitContainer();
		
		private GroupBox connectionInfoGroupBox = new GroupBox();
		private GroupBox lqrTuningGroupBox = new GroupBox();
		
		private GroupBox debugTextGroupBox = new GroupBox();
		private RichTextBox debugTextBox = new RichTextBox();
		
		/// <summary>
		/// Initializes the UI
		/// </summary>
		private void InitializeComponents()
		{
			//
			// connectionInfoGroupBox
			// 
			this.connectionInfoGroupBox.Text = "Connection";
			this.connectionInfoGroupBox.Width = 380;
			this.connectionInfoGroupBox.Height = 150;
			this.connectionInfoGroupBox.Margin = new Padding(10);
			
			//
			// lqrTuningGroupBox
			// 
			this.lqrTuningGroupBox.Text = "LQR Tuning";
			this.lqrTuningGroupBox.Width = 380;
			this.lqrTuningGroupBox.Height = 250;
			this.lqrTuningGroupBox.Margin = new Padding(10, 0, 10, 10);
			
			//
			// leftPanel
			//
			this.leftPanel.Dock = DockStyle.Left;
			this.leftPanel.Width = 400;
			this.leftPanel.FlowDirection = FlowDirection.TopDown;
			this.leftPanel.Controls.Add(this.connectionInfoGroupBox);
			this.leftPanel.Controls.Add(this.lqrTuningGroupBox);
			
			//
			// middlePanel
			//
			this.middlePanel.Dock = DockStyle.Left;
			this.middlePanel.Width = 7;
			this.middlePanel.BackColor = Color.WhiteSmoke;
			
			//
			// debugTextBox
			//
			this.debugTextBox.Dock = DockStyle.Fill;
			this.debugTextBox.BackColor = Color.White;
			this.debugTextBox.Font = new Font("Courier New", 12, FontStyle.Regular);
			this.debugTextBox.BorderStyle = BorderStyle.None;
			this.debugTextBox.ReadOnly = true;
			
			//
			// debugTextGroupBox
			//
			this.debugTextGroupBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
			this.debugTextGroupBox.Text = "Debug";
			this.debugTextGroupBox.Padding = new Padding(7);
			this.debugTextGroupBox.Controls.Add(this.debugTextBox);
			
			//
			// rightSplitContainer
			//
			this.rightSplitContainer.Dock = DockStyle.Fill;
			this.rightSplitContainer.Orientation = Orientation.Horizontal;
			this.rightSplitContainer.SplitterWidth = 7;
			this.rightSplitContainer.BackColor = Color.LightGray;
			
			//
			// rightSplitContainer.Panel1
			//
			
			//
			// rightSplitContainer.Panel2
			//
			this.rightSplitContainer.Panel2.BackColor = Color.White;
			this.rightSplitContainer.Panel2.Controls.Add(debugTextGroupBox);
			
			//
			// MainWindow
			//
			this.Text = "Stasis Command Center";
			this.WindowState = FormWindowState.Maximized;
			this.Font = new System.Drawing.Font(this.Font.FontFamily, 10, System.Drawing.FontStyle.Regular);
			this.Menu = this.mainMenu;
			this.BackColor = Color.White;
			this.Controls.Add(this.rightSplitContainer);
			this.Controls.Add(this.middlePanel);
			this.Controls.Add(this.leftPanel);
		}
	}
}

