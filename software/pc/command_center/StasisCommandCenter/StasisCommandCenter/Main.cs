using System;
using System.Windows.Forms;

namespace StasisCommandCenter
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Run(new MainWindow());
		}
	}
}