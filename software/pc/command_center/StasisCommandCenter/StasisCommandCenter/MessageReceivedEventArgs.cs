using System;

namespace StasisCommandCenter
{
	/// <summary>
	/// Message received event handler.
	/// </summary>
	public delegate void MessageReceivedEventHandler(object sender, MessageReceivedEventArgs e);
	
	/// <summary>
	/// Message type.
	/// </summary>
	public enum MessageType : byte
	{
		/// <summary>
		/// Just a debugging string
		/// </summary>
		Debug = 0x01,
		/// <summary>
		/// Bunch of doubles with a target
		/// </summary>
		Values = 0x02,
	}
	
	/// <summary>
	/// Message received event data
	/// </summary>
	public class MessageReceivedEventArgs
	{
		/// <summary>
		/// Gets the type of the message
		/// </summary>
		public MessageType Type
		{
			get;
			private set;
		}
		
		/// <summary>
		/// Gets the data in the message
		/// </summary>
		public byte[] Data
		{
			get;
			private set;
		}
		
		/// <summary>
		/// Constructor
		/// </summary>
		public MessageReceivedEventArgs(MessageType type, byte[] data)
		{
			this.Type = type;
			this.Data = data;
		}
	}
}

