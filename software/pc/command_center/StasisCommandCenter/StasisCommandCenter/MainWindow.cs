using System;
using System.Drawing;
using System.Windows.Forms;

namespace StasisCommandCenter
{
	public partial class MainWindow : Form
	{
		/// <summary>
		/// Communication link to the robot
		/// </summary>
		private CommunicationLink commLink = new CommunicationLink();
		
		/// <summary>
		/// Constructor
		/// </summary>
		public MainWindow()
		{
			// Initialize UI
			this.InitializeComponents();
			
			// Initialize comm link
			this.commLink.MessageReceived += HandleCommLinkhandleMessageReceived;
		}
		
		/// <summary>
		/// Handles messages from the comm link
		/// </summary>
		private void HandleCommLinkhandleMessageReceived(object sender, MessageReceivedEventArgs e)
		{
			
		}
	}
}

