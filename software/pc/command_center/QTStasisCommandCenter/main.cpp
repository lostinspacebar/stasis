#include <QtGui/QApplication>
#include <QFile>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    
    QFile File(":/stylesheet.qss");
    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll());
    
    qApp->setStyleSheet(StyleSheet);
    
    MainWindow w;
    w.show();
    
    return a.exec();
}
