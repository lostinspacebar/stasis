#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "debug.h"
#include <QDateTime>
#include <QList>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    // Init UI
    ui->setupUi(this);
    
    // Listen to debug messages
    QObject::connect(Debug::instance(), SIGNAL(onMessage(QtMsgType,QString*)), this, SLOT(handleDebugMessage(QtMsgType, QString *)));
    
    // Initialize comm link
    commLink = new CommunicationLink();
    QObject::connect(commLink, SIGNAL(onOpened()), this, SLOT(handleCommOpened()));
    QObject::connect(commLink, SIGNAL(onClosed()), this, SLOT(handleCommClosed()));
    QObject::connect(commLink, SIGNAL(onMessageReceived(Message*)), this, SLOT(handleCommMessageReceived(Message*)));
    
    // Load settings file
    settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, "Lost.In.Spacebar", "StasisCommandCenter");
    
    // If settings were already saved. Load them into widgets
    if(settings->contains("connection/ip"))
    {
        ui->ipAddressTextBox->setText(settings->value("connection/ip", "").toString());
    }
    if(settings->contains("connection/port"))
    {
        ui->portSpinBox->setValue(settings->value("connection/port").toInt());
    }
    if(settings->contains("lqr/a_matrix") == false)
    {
        QMatrix4x4 m;
        m.fill(0.0);
        settings->setValue("lqr/a_matrix", m);
    }
    if(settings->contains("lqr/q_matrix") == false)
    {
        QMatrix4x4 m;
        m.fill(0.0);
        settings->setValue("lqr/q_matrix", m);
    }
    if(settings->contains("lqr/k_matrix") == false)
    {
        QMatrix4x4 m;
        m.fill(0.0);
        settings->setValue("lqr/k_matrix", m);
    }
    if(settings->contains("pid/value_matrix") == false)
    {
        QMatrix4x4 m;
        m.fill(0.0);
        settings->setValue("pid/value_matrix", m);
    }
    this->loadLQRTuningFromSettings();
    this->loadPIDTuningFromSettings();
    
    // Plots
    ui->tiltPlot->addGraph();
    ui->outputPlot->addGraph();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openLink(QString ip, int port)
{
    // Open comm link. We will just wait for the opened event.
    commLink->open(ip, port);
}

void MainWindow::closeLink()
{
    // Close link. And wait for disconnected event.
    commLink->close();
}

void MainWindow::handleCommOpened()
{
    // Enable UI since we're connected
    ui->connectButton->setEnabled(false);
    ui->lqrTuningGroupBox->setEnabled(true);
    ui->pidGroupBox->setEnabled(true);
    ui->disconnectButton->setEnabled(true);
}

void MainWindow::handleCommClosed()
{
    // Enable/Disable UI
    ui->disconnectButton->setEnabled(false);
    ui->lqrTuningGroupBox->setEnabled(false);
    ui->pidGroupBox->setEnabled(false);
    ui->connectButton->setEnabled(true);
}

void MainWindow::handleCommMessageReceived(Message *message)
{
    if(message->getType() == DEBUG)
    {
        DebugMessage *debugMessage = (DebugMessage *)message;
        Debug::write(debugMessage->getMessage());
    }
    else if(message->getType() == REPORT_VALUES)
    {
        ReportValuesMessage *valuesMessage = (ReportValuesMessage *)message;
        QString *out = new QString();
        float *values = valuesMessage->getValues();
        for(int i = 0; i < valuesMessage->getCount(); i++)
        {
            QString token;
            token.sprintf("%d:%04.4f", i, values[i]);
            out->append(token);
            out->append("\t");
        }
        Debug::write(out);
        
        double x = QDateTime::currentMSecsSinceEpoch();
        
        // Plot
        ui->tiltPlot->graph(0)->addData(x, values[3]);
        ui->tiltPlot->graph(0)->valueAxis()->setRange(0, 180);
        ui->tiltPlot->graph(0)->keyAxis()->setRange(x - 10000, x);
        ui->tiltPlot->replot();
        ui->outputPlot->graph(0)->addData(x, values[5]);
        ui->outputPlot->graph(0)->valueAxis()->setRange(-3, 3);
        ui->outputPlot->graph(0)->keyAxis()->setRange(x - 10000, x);
        ui->outputPlot->replot();
    }
}

void MainWindow::handleDebugMessage(QtMsgType type, QString *message)
{    
    // Type of message
    switch(type)
    {
        case QtDebugMsg:
            message->prepend("<span>DEBUG: ");
            break;
        case QtWarningMsg:
            message->prepend("<span style='background-color:#ffff00'>WARNING:</span> ");
            break;
        case QtCriticalMsg:
            message->prepend("<span style='background-color:#ff0000; font-weight:bold;'>ERROR:</span> ");
            break;
    }
    
    // Prepend timestamp
    QDateTime now = QDateTime::currentDateTime();
    message->prepend(": ");
    message->prepend(now.toString());

    // Add to debug text box
    ui->debugTextBox->appendHtml(*message);
    ui->debugTextBox->ensureCursorVisible();
}

QMatrix4x4 MainWindow::getMatrixFromTable(QTableWidget *table)
{
    QMatrix4x4 matrix;
    for(int i = 0; i < 4; i++)
    {
        QVector4D row(table->item(i, 0)->text().toDouble(), 
                      table->item(i, 1)->text().toDouble(), 
                      table->item(i, 2)->text().toDouble(), 
                      table->item(i, 3)->text().toDouble());
        matrix.setRow(i, row);
    }
    
    return matrix;
}

void MainWindow::setTableDataFromMatrix(QTableWidget *table, QMatrix4x4 matrixData)
{
    table->clearContents();
    
    for(int i = 0; i < 4; i++)
    {
        QVector4D row = matrixData.row(i);
        double c0 = row.x();
        double c1 = row.y();
        double c2 = row.z();
        double c3 = row.w();
        table->setItem(i, 0, new QTableWidgetItem(QString::number(c0)));
        table->setItem(i, 1, new QTableWidgetItem(QString::number(c1)));
        table->setItem(i, 2, new QTableWidgetItem(QString::number(c2)));
        table->setItem(i, 3, new QTableWidgetItem(QString::number(c3)));
    }
}

void MainWindow::loadLQRTuningFromSettings()
{
    QMatrix4x4 aMatrix = settings->value("lqr/a_matrix").value<QMatrix4x4>();
    this->setTableDataFromMatrix(ui->aTable, aMatrix);
    
    QMatrix4x4 qMatrix = settings->value("lqr/q_matrix").value<QMatrix4x4>();
    this->setTableDataFromMatrix(ui->qTable, qMatrix);
    
    QMatrix4x4 kMatrix = settings->value("lqr/k_matrix").value<QMatrix4x4>();
    this->setTableDataFromMatrix(ui->kTable, kMatrix);
    
    ui->aTable->setStyleSheet("background-color:#ffffff;");
    ui->qTable->setStyleSheet("background-color:#ffffff;");
    ui->kTable->setStyleSheet("background-color:#ffffff;");
}

void MainWindow::loadPIDTuningFromSettings()
{
    QMatrix4x4 vMatrix = settings->value("pid/value_matrix").value<QMatrix4x4>();
    this->setTableDataFromMatrix(ui->pidTable, vMatrix);
    
    ui->pidTable->setStyleSheet("background-color:#ffffff;");
}

void MainWindow::sendPIDTuning()
{
    // Save settings
    QMatrix4x4 vMatrix = this->getMatrixFromTable(ui->pidTable);
    settings->setValue("pid/value_matrix", vMatrix);
    ui->pidTable->setStyleSheet("background-color:#ffffff;");
    
    // Send data to maple
    commLink->sendPIDTuning(vMatrix);
    
    Debug::write("Sending PID Tuning Parameters");
}

void MainWindow::on_connectButton_clicked()
{
    // Open link
    this->openLink(ui->ipAddressTextBox->text(), ui->portSpinBox->value());
}

void MainWindow::on_disconnectButton_clicked()
{
    // Close link
    this->closeLink();
}

void MainWindow::on_ipAddressTextBox_textEdited(const QString &arg1)
{
    // Save settings
    settings->setValue("connection/ip", ui->ipAddressTextBox->text());
}

void MainWindow::on_portSpinBox_valueChanged(int arg1)
{
    // Save settings
    settings->setValue("connection/port", ui->portSpinBox->value());
}

void MainWindow::on_applyButton_clicked()
{
    // Save settings
    QMatrix4x4 aMatrix = this->getMatrixFromTable(ui->aTable);
    settings->setValue("lqr/a_matrix", aMatrix);
    QMatrix4x4 qMatrix = this->getMatrixFromTable(ui->qTable);
    settings->setValue("lqr/q_matrix", qMatrix);
    QMatrix4x4 kMatrix = this->getMatrixFromTable(ui->kTable);
    settings->setValue("lqr/k_matrix", kMatrix);
    ui->aTable->setStyleSheet("background-color:#ffffff;");
    ui->qTable->setStyleSheet("background-color:#ffffff;");
    ui->kTable->setStyleSheet("background-color:#ffffff;");
    
    // Send data to maple
    commLink->sendLQRTuning(aMatrix, qMatrix, kMatrix);
    
    Debug::write("Sending LQR Tuning Parameters");
}

void MainWindow::on_revertButton_clicked()
{
    this->loadLQRTuningFromSettings();
}

void MainWindow::on_aTable_cellChanged(int row, int column)
{
    ui->aTable->setStyleSheet("background-color:#ff5555");
}

void MainWindow::on_qTable_cellChanged(int row, int column)
{
    ui->qTable->setStyleSheet("background-color:#ff5555");
}

void MainWindow::on_pidTable_cellChanged(int row, int column)
{
    ui->pidTable->setStyleSheet("background-color:#ff5555");
}

void MainWindow::on_revertPIDButton_clicked()
{
    this->loadPIDTuningFromSettings();
}

void MainWindow::on_applyPIDButton_clicked()
{
    this->sendPIDTuning();
}

void MainWindow::on_kTable_cellChanged(int row, int column)
{
    ui->kTable->setStyleSheet("background-color:#ff5555");
}

void MainWindow::on_clearDebugButton_clicked()
{
    ui->debugTextBox->clear();
}

void MainWindow::on_disablePIDButton_clicked()
{
    // Send dummy 0 Matrix to disable PID
    QMatrix4x4 vMatrix;
    vMatrix.fill(0);
    commLink->sendPIDTuning(vMatrix);
    
    Debug::write("Disabling PID");
}

void MainWindow::on_disableButton_clicked()
{
    // Save settings
    QMatrix4x4 matrix;
    matrix.fill(0);
    
    // Send data to maple
    commLink->sendLQRTuning(matrix, matrix, matrix);
    
    Debug::write("Disabling LQR");
}
