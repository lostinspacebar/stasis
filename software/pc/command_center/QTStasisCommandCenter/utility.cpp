#include "utility.h"

QByteArray Utility::convertToByteArray(float val)
{
    val = val * 10000.0f;
    int v = floor(val);
    QByteArray array;
    array.append((uchar)(v & 0xFF));
    array.append((uchar)((v >> 8) & 0xFF));
    array.append((uchar)((v >> 16) & 0xFF));
    array.append((uchar)((v >> 24) & 0xFF));
    
    return array;
};
