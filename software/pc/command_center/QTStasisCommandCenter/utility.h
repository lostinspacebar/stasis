/**
 * Utility
 * -------------------------------------------------------------
 * Provides some useful utility functions
 *
 */

#ifndef UTILITY_H
#define UTILITY_H

// Includes
#include <stdlib.h>
#include <string.h>
#include <QByteArray>
#include <qmath.h>

class Utility
{
public:
	
	/**
	 * Converts byte data to a 16-bit unsigned int.
	 * 
	 * @param buffer	Byte buffer (of length 2)
	 * @return			16-bit unsigned integer value
	 */
	static ushort convertToUInt16(uchar *buffer)
	{
		ushort val;
		memcpy(&val, buffer, 2);
		
		return val;
	};
	
	/**
	 * Converts byte data to a 32-bit unsigned int.
	 * 
	 * @param buffer	Byte buffer (of length 4)
	 * @return			32-bit unsigned integer value
	 */
	static uint convertToUInt32(uchar *buffer)
	{
		uint val;
		memcpy(&val, buffer, 4);
		
		return val;
	};
	
	/**
	 * Converts byte data to a 16-bit signed int.
	 * 
	 * @param buffer	Byte buffer (of length 2)
	 * @return			16-bit signed integer value
	 */
	static short convertToInt16(uchar *buffer)
	{
		short val;
		memcpy(&val, buffer, 2);
		
		return val;
	};
	
	/**
	 * Converts byte data to a 32-bit signed int.
	 * 
	 * @param buffer	Byte buffer (of length 4)
	 * @return			32-bit signed integer value
	 */
	static int convertToInt32(uchar *buffer)
	{
		int val;
		memcpy(&val, buffer, 4);
		
		return val;
	};
	
	/**
	 * Converts byte data to a 32-bit float
	 * 
	 * @param buffer	Byte buffer (of length 4)
	 * @return			32-bit float value
	 */
	static float convertToFloat(uchar *buffer)
	{
        return ((float)convertToInt32(buffer)) / 10000.0;
	};
	
	/**
	 *  Converts a float value to an array of bytes. The data 
	 *  is stored in the "buffer" variable passed in.
	 *  
	 *  @param val		Value to convert
	 */
	static QByteArray convertToByteArray(float val);
	
};

#endif
