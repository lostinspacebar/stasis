#ifndef DEBUG_H
#define DEBUG_H

#include <QObject>
#include <QString>

class Debug : public QObject
{
    Q_OBJECT
    
public:
    
    /**
     * Gets the singleton instance of the debug message stream
     *
     * @return Debug instance
     */
    static Debug *instance();
    
    /**
     * Writes a message of the specified type to the debug stream
     *
     * @param type      Type of message
     * @param message   Message contents
     *
     */
    static void write(QtMsgType type, QString *message);
    
    /**
     * Writes a message of the specified type to the debug stream
     *
     * @param type      Type of message
     * @param message   Message contents
     *
     */
    static void write(QtMsgType type, const char *message)
    {
        Debug::write(type, new QString(message));
    }
    
    /**
     * Writes a message to the debug stream. Identical to 
     * WriteMessage()
     *
     * @param message   Non-warning/error message.
     *
     */
    static void write(QString *message)
    {
        Debug::write(QtDebugMsg, message);
    }
    
    /**
     * Writes a message to the debug stream. Identical to 
     * WriteMessage()
     *
     * @param message   Non-warning/error message.
     *
     */
    static void write(const char *message)
    {
        Debug::write(QtDebugMsg, message);
    }
    
    /**
     * Writes a message to the debug stream
     *
     * @param message   Non-warning/error message.
     *
     */
    static void writeMessage(const char *message)
    {
        Debug::write(QtDebugMsg, message);
    }

    /**
     * Writes a warning message to the debug stream
     *
     * @param message   Warning message
     *
     */
    static void writeWarning(const char *message)
    {
        Debug::write(QtWarningMsg, message);
    }
    
    /**
     * Writes an error message to the debug stream
     *
     * @param message   Warning message
     *
     */
    static void writeError(const char *message)
    {
        Debug::write(QtCriticalMsg, message);
    }
    
private:
    
    /**
     * Writes a message of the specified type to the debug stream
     *
     * @param type      Type of message
     * @param message   Message contents
     *
     */
    void writeInternal(QtMsgType type, QString *message);
    
    /**
     * Singleton instance
     */
    static Debug *_singleton;
    
signals:
    
    /**
     * Raised when there is a new message written to the debug stream
     *
     * @param type      Type of message
     * @param message   Message written
     */
    void onMessage(QtMsgType type, QString *message);
};

#endif // DEBUG_H
