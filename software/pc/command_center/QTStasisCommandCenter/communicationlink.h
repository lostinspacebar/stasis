#ifndef COMMUNICATIONLINK_H
#define COMMUNICATIONLINK_H

#include <QObject>
#include <QString>
#include <QtNetwork/QTcpSocket>
#include <QMatrix4x4>
#include "message.h"

class CommunicationLink : public QObject
{
    Q_OBJECT
    
public:
    
    /**
      * Connects to the specified IP and port
      *
      * @param ip       IP Address to connect to
      * @param port     Port number on IP address to connect to
      *
      */
    void open(QString ip, int port);
    
    /**
     * Closes connection if there is one open
     *
     */
    void close();
    
    /**
     * Sends LQR tuning paramters to the robot
     *
     */
    void sendLQRTuning(QMatrix4x4 aMatrix, QMatrix4x4 qMatrix, QMatrix4x4 kMatrix);
    
    /**
     * Sends PID tuning parameters to the robot
     *
     */
    void sendPIDTuning(QMatrix4x4 pidMatrix);
    
signals:
    
    /**
     * Raised when a connection has been opened successfully
     *
     */
    void onOpened();
    
    /**
     * Raised when a connection has closed
     *
     */
    void onClosed();
    
    /**
     * Raised when a message has been received from the robot
     * 
     * @param message   Message received
     *
     */
    void onMessageReceived(Message *message);
    
private slots:
    
    /**
      * Handles successful connection to the host.
      */
    void handleSocketConnected();
    
    /**
      * Handles disconnection from host.
      */
    void handleSocketDisconnected();
    
    /**
     * Handle socket error
     *
     * @param error     Socket error information
     */
    void handleSocketError(QAbstractSocket::SocketError error);
    
    /**
      * Handles new data when it becomes available on the socket/stream.
      *
      */
    void handleSocketData();
    
private:
    
    /**
      * Handles a new message packet received over the comm link
      *
      */
    void handleMessage(uchar *buffer, uint length);
    
    // Socket used for communication
    QTcpSocket *socket;
    
    uchar _buffer[256];
    int _bufferLength;
    
};

#endif // COMMUNICATIONLINK_H
