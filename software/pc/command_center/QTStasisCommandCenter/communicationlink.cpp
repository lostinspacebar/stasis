#include "debug.h"
#include "communicationlink.h"
#include "utility.h"
#include "message.h"
#include <QApplication>
#include <QMetaEnum>
#include <QList>
#include <qmath.h>

void CommunicationLink::open(QString ip, int port)
{
    // Create a new socket
    socket = new QTcpSocket();
    
    // Connect up some signals
    QObject::connect(socket, SIGNAL(connected()), this, SLOT(handleSocketConnected()));
    QObject::connect(socket, SIGNAL(readyRead()), this, SLOT(handleSocketData()));
    QObject::connect(socket, SIGNAL(disconnected()), this, SLOT(handleSocketDisconnected()));
    QObject::connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(handleSocketError(QAbstractSocket::SocketError)));
    
    // Connect to IP/Port
    socket->connectToHost(ip, port);
}

void CommunicationLink::close()
{
    if(socket != NULL)
    {
        socket->close();
    }
}

void CommunicationLink::sendLQRTuning(QMatrix4x4 aMatrix, QMatrix4x4 qMatrix, QMatrix4x4 kMatrix)
{
    QByteArray message;
    
    // Add header
    message.append(0xFF);
    message.append(0xFF);
    message.append(0xFF);
    message.append(0xFF);
    
    // Message type
    message.append(0x44);
    
    // Add A Data
    for(int i = 0; i < 4; i++)
    {
        for(int p = 0; p < 4; p++)
        {
            message.append(Utility::convertToByteArray((float)aMatrix(i, p)));
        }
    }
    
    // Add Q Data
    for(int i = 0; i < 4; i++)
    {
        for(int p = 0; p < 4; p++)
        {
            message.append(Utility::convertToByteArray((float)qMatrix(i, p)));
        }
    }
    
    // Add K Data
    for(int i = 0; i < 4; i++)
    {
        for(int p = 0; p < 4; p++)
        {
            message.append(Utility::convertToByteArray((float)kMatrix(i, p)));
        }
    }
    
    // Add footer
    message.append(0xFF);
    message.append(0xFF);
    message.append(0xFF);
    message.append(0xFE);
    
    // Write to socket
    socket->write(message);
}

void CommunicationLink::sendPIDTuning(QMatrix4x4 pidMatrix)
{
    QByteArray message;
    
    // Add header
    message.append(0xFF);
    message.append(0xFF);
    message.append(0xFF);
    message.append(0xFF);
    
    // Message type
    message.append(0x45);
    
    // Add PID Data
    for(int i = 0; i < 4; i++)
    {
        for(int p = 0; p < 4; p++)
        {
            message.append(Utility::convertToByteArray((float)pidMatrix(i, p)));
        }
    }
    
    // Add footer
    message.append(0xFF);
    message.append(0xFF);
    message.append(0xFF);
    message.append(0xFE);
    
    // Write to socket
    socket->write(message);
}

void CommunicationLink::handleMessage(uchar *buffer, uint length)
{
    Message *message;
    MessageType type = (MessageType)(buffer[4]);
    if(type == DEBUG)
    {
        message = new DebugMessage(buffer, length);
    }
    else if(type == REPORT_VALUES)
    {
        message = new ReportValuesMessage(buffer, length);
    }
    
    if(message != NULL)
    {
        // Raise signal
        emit onMessageReceived(message);   
    }
}

void CommunicationLink::handleSocketConnected()
{
    Debug::write("Communication Link Open");
    
    // Let peeps know
    emit onOpened();
}

void CommunicationLink::handleSocketDisconnected()
{
    Debug::write("Communication Link Closed");
    
    // Let peeps know
    emit onClosed();
}

void CommunicationLink::handleSocketError(QAbstractSocket::SocketError error)
{
    Debug::writeError(socket->errorString().toLocal8Bit().data());
}

void CommunicationLink::handleSocketData()
{
    
    while(socket->bytesAvailable() > 0)
    {
        // Temporary buffer to read in a byte
        char tmpBuffer[1];
        
        // Read in a byte
        qint64 bytesRead = socket->read(tmpBuffer, 1);
        
        // Process byte
        if(bytesRead > 0)
        {
            uchar b = (uchar)tmpBuffer[0];
            if(_bufferLength < 4)
            {
                // Still waiting for start headers
                if(b == 0xFF)
                {
                    _buffer[_bufferLength++] = b;
                }
                else
                {
                     _bufferLength = 0;
                }
            }
            else
            {
                // Save byte
                _buffer[_bufferLength++] = b;
                
                // See if we have an end message yet
                if(_buffer[_bufferLength - 1] == 0xFE && 
                        _buffer[_bufferLength - 2] == 0xFF &&
                        _buffer[_bufferLength - 3] == 0xFF &&
                        _buffer[_bufferLength - 4] == 0xFF)
                {
                    // Message received. Handle it
                    this->handleMessage(_buffer, _bufferLength);
                    
                    // Reset buffer
                    _bufferLength = 0;
                }
            }
        }
    }
}
