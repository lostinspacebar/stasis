#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QTableWidget>
#include <QMainWindow>
#include <QMatrix4x4>
#include <QSettings>
#include "communicationlink.h"
#include "message.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    
    /**
     * Handle comm link being opened
     *
     */
    void handleCommOpened();
    
    /**
     * Handle comm link being closed
     *
     */
    void handleCommClosed();
    
    /**
     * Handle message being received
     *
     */
    void handleCommMessageReceived(Message *message);
    
    /**
     * Handle debugging messages sent from qDebug
     *
     * @param type  Type of message (ERROR, WARNING etc.)
     * @param msg   Message of debug call
     *
     */
    void handleDebugMessage(QtMsgType type, QString *message);
    
    //
    // UI Event Handlers
    //
    
    void on_connectButton_clicked();
    void on_disconnectButton_clicked();
    void on_ipAddressTextBox_textEdited(const QString &arg1);
    void on_portSpinBox_valueChanged(int arg1);
    void on_applyButton_clicked();
    void on_revertButton_clicked();
    
    void on_aTable_cellChanged(int row, int column);
    
    void on_qTable_cellChanged(int row, int column);
    
    void on_pidTable_cellChanged(int row, int column);
    
    void on_revertPIDButton_clicked();
    
    void on_applyPIDButton_clicked();
    
    void on_kTable_cellChanged(int row, int column);
    
    void on_clearDebugButton_clicked();
    
    void on_disablePIDButton_clicked();
    
    void on_disableButton_clicked();
    
private:
    
    /**
     * Opens a link to the robot
     *
     */
    void openLink(QString ip, int port);
    
    /**
     * Closes link to robt
     *
     */
    void closeLink();
    
    /**
     * Creates a matrix from table data
     *
     * @param table     Table to get matrix data from
     * @return          Matrix4x4 version of data
     */
    QMatrix4x4 getMatrixFromTable(QTableWidget *table);
    
    /**
     * Sets table data to one given in the matrix
     *
     * @param table         Table to fill with matrix data
     * @param matrixData    Data to set in the table
     */
    void setTableDataFromMatrix(QTableWidget *table, QMatrix4x4 matrixData);
    
    /**
     * Reverts LQR tuning data
     *
     */
    void loadLQRTuningFromSettings();
    
    /**
     * Reverts PID tuning data
     *
     */
    void loadPIDTuningFromSettings();
    
    /**
     * Send PID tuning parameters set in the UI
     *
     */
    void sendPIDTuning();
    
    // UI instance
    Ui::MainWindow *ui;
    
    // Communication Link instance
    CommunicationLink *commLink;
    
    // Settings instance
    QSettings *settings;
};

#endif // MAINWINDOW_H
