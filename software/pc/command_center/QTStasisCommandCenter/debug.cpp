#include "debug.h"

Debug *Debug::instance()
{  
    // Make sure singleton exists
    if(_singleton == NULL)
    {
        _singleton = new Debug();
    }
    
    // Return the instance
    return _singleton;
}

void Debug::write(QtMsgType type, QString *message)
{    
    // Call internal write function
    instance()->writeInternal(type, message);
}

void Debug::writeInternal(QtMsgType type, QString *message)
{
    // Let all slots know about message
    emit onMessage(type, message);
}

/**
 * Singleton instance
 */
Debug *Debug::_singleton = new Debug();
