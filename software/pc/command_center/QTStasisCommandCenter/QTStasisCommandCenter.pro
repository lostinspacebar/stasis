#-------------------------------------------------
#
# Project created by QtCreator 2012-09-05T21:57:43
#
#-------------------------------------------------

QT       += core gui network

TARGET = QTStasisCommandCenter
TEMPLATE = app


SOURCES +=  main.cpp\
            mainwindow.cpp \
            communicationlink.cpp \
            debug.cpp \
    utility.cpp \
    message.cpp \
    qcustomplot.cpp

HEADERS  += mainwindow.h \
            communicationlink.h \
            debug.h \
    utility.h \
    message.h \
    qcustomplot.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    stylesheet.qss

RESOURCES += \
    resources.qrc
