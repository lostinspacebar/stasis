#ifndef MESSAGE_H
#define MESSAGE_H

#include <QString>
#include "utility.h"

enum MessageType
{
    DEBUG = 0x55,
    REPORT_VALUES = 0x46
};

class Message
{

public:
    
    MessageType getType()
    {
        return _type;
    }
    
    int getLength()
    {
        return _dataLength;
    }
    
protected:
    
    // Type of message
    MessageType _type;
    
    // Data in the message
    uchar data[256];
    
    // Length of message
    int _dataLength;
    
};

class DebugMessage : public Message
{
public:
    DebugMessage(uchar *data, int dataLength)
    {
        // Save type
        _type = DEBUG;
        
        char msg[dataLength - 9];
        memcpy(msg, data + 5, dataLength - 9);
        
        // Set message
        _message = new QString(msg);
    }
    
    QString *getMessage()
    {
        return _message;
    }
    
private:
    
    QString *_message;
};

class ReportValuesMessage : public Message
{
public:
    ReportValuesMessage(uchar *data, int dataLength)
    {
        // Save type
        _type = REPORT_VALUES;
        
        int count = 5;
        int numValues = Utility::convertToInt16(data + count);
        count += 2;
        
        for(int i = 0; i < numValues; i++)
        {
            _values[i] = Utility::convertToFloat(data + count);
            count += 4;
        }
        
        _count = numValues;
    }
    
    float *getValues()
    {
        return _values;
    }
    
    int getCount()
    {
        return _count;
    }
    
private:
    
    float _values[10];
    int _count;
};

#endif // MESSAGE_H
