/**
 * Statis Robot Controller
 * -------------------------------------------------------------
 * Main controller for the stasis robot logic
 *
 */

// Includes
#include <stdio.h>
#include <string.h>
#include "WProgram.h"
#include "StasisRobot.h"
#include "StasisController.h"
#include "AveragingFilter.h"
#include "MedianFilter.h"
#include "CommunicationLink.h"

/**
 * Initialize the controller instance
 *
 * @param robot    Robot that this controller will be err... controlling.
 *
 */
void StasisController::initialize(StasisRobot *robot)
{
	// Save robot instance for later
	this->_robot = robot;
	
	_loopTime = 15;
	_lastCall = 0;
}

/**
 * Let's the controller perform a think iteration.
 *
 */
void StasisController::think()
{
	// Averaging filter
	static MedianFilter medFilter = MedianFilter(5);
	static AveragingFilter avgFilter = AveragingFilter(3);
	
	// Maintain loop time
	uint32 now = systick_uptime();
	if(now - _lastCall < _loopTime)
	{
		return;
	}
	
	// Called.
	_lastCall = now;
	
	// Update sensors on the robot
	this->_robot->update(_loopTime);
	
	// Update medFilter
	float tiltX = avgFilter.update(medFilter.update(this->_robot->getTilt().X));
	float x = this->_robot->getDisplacement().Y;
	float velocity = (x / 1000.0f) / (_loopTime / 1000.0f);
	float angularVelocity = (tiltX - _lastTiltX) / (_loopTime / 1000.0f);
	
	// Save tilt for next iteration
	_lastTiltX = tiltX;
	
	_xMatrix.setValue(0, 0, x);
	_xMatrix.setValue(1, 0, velocity);
	_xMatrix.setValue(2, 0, tiltX - _anglePID.SetPoint);
	_xMatrix.setValue(3, 0, angularVelocity);
	
	// Output 0 by default for safety
	float out = 0;
	
	// Caculate output if tilt is within safe limits
	if(tiltX > 45 && tiltX < 135)
	{
		// Update PID controller with new values
		float anglePidOut = this->_anglePID.update(tiltX);
		float displacementPidOut = this->_displacementPID.update(x);
		float velocityPidOut = this->_velocityPID.update(velocity);
		float angularVelocityPidOut = this->_angularVelocityPID.update(angularVelocity);
		
		// Update LQR
		float lqrOut = _lqr.update(_xMatrix);
		
		// Out is combo of all
		out = anglePidOut + displacementPidOut + velocityPidOut + angularVelocityPidOut + lqrOut;
		
		// Normalize
		out = out / -180.0;
	}
	
	float report[] = { this->_fps, x, velocity, tiltX, angularVelocity, out };
	CommunicationLink::writeMessage(ReportValuesMessage(report, 6));
	
	// Set motors
	this->_robot->setThrottle(out, out);
	
	// Done with a "frame". Update FPS
	this->calculateFps();
}

void StasisController::handlePIDMessage(PIDTuningMessage *message)
{
	_displacementPID.tune(message->getDisplacementTuning());
	_anglePID.tune(message->getAngleTuning());
	_velocityPID.tune(message->getVelocityTuning());
	_angularVelocityPID.tune(message->getAngularVelocityTuning());
	
	// Reset robot so it can keep calculating stuff right
	_robot->reset();
}

void StasisController::handleLQRMessage(LQRTuningMessage *message)
{
	_lqr.setKMatrix(message->getKMatrix());
}

/**
 * Calculate average fps
 */
void StasisController::calculateFps()
{
	// Increment fps counter
	this->_fpsTemp++;
	
	// Check to see if we should update FPS
	uint32 now = systick_uptime();
	uint32 diff = now - this->_lastFpsTimer;
	if(this->_lastFpsTimer > now)
	{
		diff = (0xFFFFFFFF - this->_lastFpsTimer) + now;
	}
	if(diff >= 1000)
	{
		this->_lastFpsTimer = now;
		this->_fps = this->_fpsTemp;
		this->_fpsTemp = 0;
	}
}
