/**
 * Communication Link
 * -------------------------------------------------------------
 * Provides functionality to communicate with the command center
 * application running on a PC over WiFi or USB-Serial
 *
 * Values that are upto 4 bytes long can be sent safely (since 
 * the header/footer are 4 bytes long. Reserved values are 
 * 0xFFFFFFFF and 0xFFFFFFFE
 * 
 */

// Includes
#include "CommunicationLink.h"

/**
 * Initializes the communication link
 * 
 * @param wifiSerialPort	Serial port to use to communicate over the WiFi module
 * @param useUSBIfAvailable Determines whether the USB serial port is used if connected.
 * @param rxHandler			Handler for messages received from the command center
 * 
 */
void CommunicationLink::initialize(HardwareSerial *wifiSerialPort, bool useUSBIfAvailable, MessageHandler rxHandler)
{
	// Save port for later
	this->_port = wifiSerialPort;
	
	// Save rx handler
	this->_rxHandler = rxHandler;
	
	// Open serial port
	this->_port->begin(230400);
}

/**
 * Read in any available data on the link
 * 
 */
void CommunicationLink::read()
{
	if(this->_port->available() > 0)
	{
		// Read in a byte
		uint8 b = this->_port->read();
		
		// Have start bytes? (0xFF, 0xFF, 0xFF, 0xFF)
		if(_bufferLength < 4)
		{
			if(b == 0xFF)
			{
				_buffer[_bufferLength++] = b;
			}
			else
			{
				_bufferLength = 0;
			}
		}
		else
		{
			_buffer[_bufferLength++] = b;
			
			// Check for end bytes
			if( _buffer[_bufferLength - 1] == 0xFE && 
				_buffer[_bufferLength - 2] == 0xFF && 
				_buffer[_bufferLength - 3] == 0xFF && 
				_buffer[_bufferLength - 4] == 0xFF)
			{
				// End of message				
				// Call the RX handler if we have one.
				if(this->_rxHandler != NULL)
				{
					// Create message from data
					MessageType type = (MessageType)_buffer[4];
					switch(type)
					{
					case LQR_TUNING:
						this->_rxHandler(&LQRTuningMessage(_buffer + 4, _bufferLength - 9));
						break;
					case PID_TUNING:
						this->_rxHandler(&PIDTuningMessage(_buffer + 4, _bufferLength - 9));
						break;
					}

					// Reset buffer state
					_bufferLength = 0;
				}
			}
		}
	}
}

void CommunicationLink::writeMessage(Message message)
{
	// Write header
	instance()->_port->write(0xFF);
	instance()->_port->write(0xFF);
	instance()->_port->write(0xFF);
	instance()->_port->write(0xFF);
	
	// Write type
	instance()->_port->write(message.getType());
	
	// Write data
	uint8 *data = message.getData();
	for(int i = 0; i < message.getLength(); i++)
	{
		instance()->_port->write(data[i]);
	}
	
	// Write footer
	instance()->_port->write(0xFF);
	instance()->_port->write(0xFF);
	instance()->_port->write(0xFF);
	instance()->_port->write(0xFE);
	
	// Flush out data
	instance()->_port->flush();
}

CommunicationLink *CommunicationLink::_instance = NULL;
