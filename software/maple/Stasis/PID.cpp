/**
 * PID Controller class
 * ----------------------------------------
 * 
 */

#include <wirish.h>
#include "PID.h"

PID::PID()
{
	P = 0;
	I = 0;
	D = 0;
	
	SetPoint = 90.0;
	
	_accumulatedError = 0;
	_integrationWindowSize = 10;
	_integrationWindowStartCursor = 0;
	_integrationWindowCount = 0;
}


PID::PID(float p, float i, float d, float setPoint)
{
	P = p;
	I = i;
	D = d;
	
	SetPoint = setPoint;
	
	_accumulatedError = 0;
	_integrationWindowSize = 10;
	_integrationWindowStartCursor = 0;
	_integrationWindowCount = 0;
}

float PID::update(float newValue)
{
	// Calculate new error
	float newError = newValue - SetPoint;
	
	// Add to accumulated error
	_integrationWindow[_integrationWindowWriteCursor++] = newError;
	_accumulatedError += newError;
	_integrationWindowCount++;
	
	// Make sure write cursor stays in bounds
	if(_integrationWindowWriteCursor >= _integrationWindowSize)
	{
		_integrationWindowWriteCursor = 0;
	}
	
	// Make sure integrator window is valid
	while(_integrationWindowCount >= _integrationWindowSize)
	{
		// Remove oldest error stored from accumulated error
		_accumulatedError -= _integrationWindow[_integrationWindowStartCursor++];
		
		// Make sure start index is valid
		if(_integrationWindowStartCursor >= _integrationWindowSize)
		{
			_integrationWindowStartCursor = 0;
		}
		
		// One less
		_integrationWindowCount--;
	}
	
	// Threshold accumulated error
	if(_accumulatedError > 100)
	{
		_accumulatedError = 100;
	}
	else if(_accumulatedError < -100.0)
	{
		_accumulatedError = -100;
	}
	
	// Calculate terms
	_pError = P * newError;
	_iError = I * _accumulatedError;
	_dError = D * (newError - _currentError);
	
//	SerialUSB.print(newError);
//	SerialUSB.print("\t");
//	SerialUSB.print(_currentError);
//	SerialUSB.print("\t");
//	//SerialUSB.print(_pError);
//	//SerialUSB.print("\t");
//	//SerialUSB.print(_iError);
//	//SerialUSB.print("\t");
//	SerialUSB.print(_dError);
//	SerialUSB.print("\t");
	
	// Store current error
	_currentError = newError;
	
	// Set output
	_output = _pError + _iError + _dError;
	
	// Return output
	return _output;
}
