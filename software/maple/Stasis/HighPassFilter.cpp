/*
 * HighPassFilter.cpp
 *
 *  Created on: Sep 21, 2012
 *      Author: aditya
 */

#include "HighPassFilter.h"

float HighPassFilter::update(float newValue)
{
	_data[_dataCursor++] = newValue;
	if(_dataCursor >= _dataSize)
	{
		_dataCursor = 0;
	}
	
	
	float subtract = 0;
	float middle = 0;
	for(int i = 0; i < _dataSize; i++)
	{
		int p = (i + _dataCursor) % _dataSize;
		if(i == 2)
		{
			middle = _data[p];
		}
		else
		{
			subtract += _data[p];
		}
	}
	/*
	  	SerialUSB.print(newValue);
		SerialUSB.print("\t");
	SerialUSB.print(_data[0]);
	SerialUSB.print("\t");
	SerialUSB.print(_data[1]);
		SerialUSB.print("\t");
		SerialUSB.print(_data[2]);
			SerialUSB.print("\t");
			SerialUSB.print(_data[3]);
				SerialUSB.print("\t");
				SerialUSB.println(_data[4]);
	 */
	
	_output = (middle * (float)_order) - subtract;
	return _output;
}


