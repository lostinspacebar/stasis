/**
 * Statis Robot
 * -------------------------------------------------------------
 * Allows interfacing with hardware on the Robot. No real 
 * control logic here.
 *
 */

// Includes
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "WProgram.h"
#include "StasisRobot.h"
#include "KalmanFilter.h"

/**
 * Initializes the stasis robot instance
 *
 * @param accel				Accelerometer instance
 * @param gyro				Gyro instance
 * @param leftEncoder		Left motor encoder instance
 * @param rightEncoder		Right motor encoder instance
 * @param leftMotor			Left motor instance
 * @param rightMotor		Right motor instance
 *
 */
void StasisRobot::initialize(AnalogAccelerometer *accel, AnalogGyro *gyro,
							 QuadratureEncoder *leftEncoder, QuadratureEncoder *rightEncoder,
							 Motor *leftMotor, Motor *rightMotor)
{
	this->_accel = accel;
	this->_gyro = gyro;
	this->_leftEncoder = leftEncoder;
	this->_rightEncoder = rightEncoder;
	this->_leftMotor = leftMotor;
	this->_rightMotor = rightMotor; 
	this->_tilt.X = 0;
}

/**
 * Update sensors and related state on the robot
 *
 */
void StasisRobot::update(uint32 loopTime)
{
	static KalmanFilter kalmanFilter = KalmanFilter(0.001, 0.003, 0.03);
	
	// Update actual sensors first
	this->_accel->update();
	this->_gyro->update();
	this->_leftEncoder->update();
	this->_rightEncoder->update();
	
	// Calculate tilt
	Vector accel = this->_accel->getValue();
	Vector gyro = this->_gyro->getValue();
	float angleX = calculateAxisTilt(accel.Y, accel.Z);
	_tilt.X = kalmanFilter.update(angleX, gyro.X, loopTime);
	
	// Update displacement
	_displacement.Y = this->_leftEncoder->getDisplacement();
}

void StasisRobot::reset()
{
	
}

/**
 * Calculates tilt around a single axis given acceleration vector components
 * 
 * @param x		Acceleration component in the "x" direction
 * @param y 	Acceleration component in the "y" direction
 * 
 * @return		Tilt in a single axis in degrees
 * 
 */
float StasisRobot::calculateAxisTilt(float x, float y)
{	
	// Return angle in degrees
	return atan2(y, x) * (180 / PI);
}

