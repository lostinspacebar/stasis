/**
 * Quadrature Encoder
 * -------------------------------------------------------------
 * Class for interfacing with quarature encoders with two 
 * pulse outputs
 *
 */

// Includes
#include "QuadratureEncoder.h"
#include <stdio.h>
#include <string.h>
#include "WProgram.h"

/**
 * Intializes the quadrature encoder instance
 *
 * @param outputAPin    		Output pin for Hall Sensor A
 * @param outputBPin    		Output pin for Hall Sensor B
 * @param countsPerRevolution	Counts per revolution of the output shaft 
 * 								(after the gear box)
 * @param wheelDiameter			Wheel diameter in millimeters
 *
 */
void QuadratureEncoder::initialize(uint8 outputAPin, uint8 outputBPin, float countsPerRevolution, uint16 wheelDiameter)
{
	// Save shit for later
	this->_outputAPin = outputAPin;
	this->_outputBPin = outputBPin;
	this->_countsPerRevolution = countsPerRevolution;
	this->_wheelCircumference = PI * wheelDiameter;

	// Set up pin modes
	pinMode(this->_outputAPin, INPUT);
	pinMode(this->_outputBPin, INPUT);

	// Initialize interrupt for hall effect outputA
	// We only need a rising edge for this pin.
	// We COULD look for both rising and falling to get more resolution,
	// but for now, this is fine.
	attachInterrupt(this->_outputAPin, EncoderInterruptHandler, this, RISING);

}

/**
 * Updates the encoder count since the last time Update() was called
 *
 */
void QuadratureEncoder::update()
{
	// Update encoder count state
	this->_encoderCount = this->_runningEncoderCount;

	// Reset running encoder count
	this->_runningEncoderCount = 0;
}

/**
 * Returns the encoder count since the last call to Update().
 *
 * @return Encoder count
 *
 */
long QuadratureEncoder::getValue()
{
	// Return count since last call to Update()
	return this->_encoderCount;
}

/**
 * Calculates and returns the displacement since the last call 
 * to update using the specified wheel diameter.
 * 							
 * @return	Return the displacement in millimeters
 */
float QuadratureEncoder::getDisplacement()
{
	return (((float)this->_encoderCount) / this->_countsPerRevolution) * this->_wheelCircumference;
}

/**
 * Interrupt handler for the hall effect output on the encoder
 *
 */
void QuadratureEncoder::EncoderInterruptHandler(void *instance)
{
	QuadratureEncoder *encoder = (QuadratureEncoder *) instance;

	// If outputB is high, we are going "forward".
	// If outputB is low, we are going "reverse"
	// These are just conventions of course, based on the actual mounting
	// and pulses of the encoders, these could be reversed.
	// Use the encoder count values appropriately
	if (digitalRead(encoder->_outputBPin) == HIGH)
	{
		encoder->_runningEncoderCount++;
	}
	else
	{
		encoder->_runningEncoderCount--;
	}
}
