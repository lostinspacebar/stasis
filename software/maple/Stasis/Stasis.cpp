/**
 * Stasis - Balancing Robot
 * -------------------------------------------------------------
 * Main driver file
 *
 */

// Includes
#include <wirish.h>
#include "AnalogAccelerometer.h"
#include "AnalogGyro.h"
#include "Motor.h"
#include "QuadratureEncoder.h"
#include "StasisRobot.h"
#include "StasisController.h"
#include "CommunicationLink.h"
#include "Utility.h"

// Sensor instances
AnalogAccelerometer accel;
AnalogGyro gyro;

// Motors
Motor leftMotor;
Motor rightMotor;

// Encoders
QuadratureEncoder leftEncoder;
QuadratureEncoder rightEncoder;

// Robot instance
StasisRobot robot;

// Controller instance
StasisController controller;

// RX Handler
void messageHandler(Message *message);

/**
 * Setup hardware / pins
 *
 */
void setup()
{	
	delay(1000);
	
	pinMode(20, OUTPUT);
	
    // Initialize accelerometer
    accel.initialize(15, 16, 17, 1400, 280);
    accel.setGain(1, -1, -1);
    accel.setTrim(0, -0.1, 0.05);
    
    // Initialize gyro
    gyro.initialize(18, 19, 1350, 2);
    gyro.setGain(1, 1);
    gyro.setTrim(-8.4, 0);

    // Initialize encoders
    // The encoders put out 16 counts per revolution of motor shaft
    // The gear ratio is 102.083:1. So counts per revolution
    // at the gearbox output is 102.083 * 16 = 1633.328
    leftEncoder.initialize(7, 6, 1633.328, 10);
    rightEncoder.initialize(5, 4, 1633.328, 10);

    // Initialize motors
    leftMotor.initialize(11, 13, 12, false);
    rightMotor.initialize(8, 9, 10, true);
    
    // Initialize the robot instance
    robot.initialize(&accel, &gyro, &leftEncoder, &rightEncoder, &leftMotor, &rightMotor);

    // Initialize the controller
    controller.initialize(&robot);
    
    // Initialize the command link
    CommunicationLink::instance()->initialize(&Serial2, false, messageHandler);
    
    // Startup complete
    CommunicationLink::writeMessage(DebugMessage("Hello. This is Stasis Version 1.0."));
}

/**
 * Thinking loop
 *
 */
void loop()
{
	// Check comm link
	CommunicationLink::instance()->read();
	
    // Let the controller go through a single iteration
    controller.think();
}

/**
 * Handle a message from the PC
 * 
 * @param message	Message received.
 */
void messageHandler(Message *message)
{
	SerialUSB.println(message->getType(), 16);
	if(message->getType() == PID_TUNING)
	{
		PIDTuningMessage *pidMessage = (PIDTuningMessage *)message;
		controller.handlePIDMessage(pidMessage);
	}
	else if(message->getType() == LQR_TUNING)
	{
		LQRTuningMessage *lqrMessage = (LQRTuningMessage *)message;
		controller.handleLQRMessage(lqrMessage);
	}
}


// Standard libmaple init() and main.
//
// The init() part makes sure your board gets set up correctly. It's
// best to leave that alone unless you know what you're doing. main()
// is the usual "call setup(), then loop() forever", but of course can
// be whatever you want.

__attribute__((constructor)) void premain()
{
	init();
}

int main(void)
{
	setup();

	while (true)
	{
		loop();
	}

	return 0;
}
