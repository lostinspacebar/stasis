/**
 * Dual Axis Analog Sensor
 * -------------------------------------------------------------
 * Class that interfaces with analog sensors that measure stuff
 * in 2 axes, where each analog output has an associated analog 
 * pin.
 *
 */

// Includes
#include <stdio.h>
#include <string.h>
#include "wirish.h"
#include "DualAxisAnalogSensor.h"
#include "WProgram.h"

/**
 * Initializes the analog sensor instance
 *
 * @param xpin            Analog X input
 * @param ypin            Analog Y input
 * @param centerVoltage   Voltage that the sensor puts out a zero-value in millivolts
 * @param sensitivity     Sensitivity of the sensor in mV/sensor-unit
 *
 */
void DualAxisAnalogSensor::initialize(uint8 xpin, uint8 ypin, float centerVoltage, float sensitivity)
{
	// Save pins for later
	this->_xpin = xpin;
	this->_ypin = ypin;

	// Save ranges for later
	this->_centerVoltage = centerVoltage;
	this->_sensitivity = sensitivity;

	// Set pin modes
	pinMode(this->_xpin, INPUT_ANALOG);
	pinMode(this->_ypin, INPUT_ANALOG);

	// Trim is 0 by default
	this->_trim.X = 0;
	this->_trim.Y = 0;
	
	// Gain is 1 by default
	this->_gain.X = 1.0;
	this->_gain.Y = 1.0;
}

/**
 * Updates values from all analog pins
 *
 */
void DualAxisAnalogSensor::update()
{
	// Get raw readings
	float x = (float) analogRead(this->_xpin);
	float y = (float) analogRead(this->_ypin);
	
	// Convert to voltage
	x = (x / 4095.0) * 3300.0;
	y = (y / 4095.0) * 3300.0;

	// Convert to actual data values
	this->_value.X = (((x - this->_centerVoltage) / this->_sensitivity) * this->_gain.X) + this->_trim.X;
	this->_value.Y = (((y - this->_centerVoltage) / this->_sensitivity) * this->_gain.Y) + this->_trim.Y;
}

/**
 * Sets the trim values for the sensor. Update() has to be called 
 * before these trim values are applied to the output value.
 *
 * @param x     Trim in the x-axis
 * @param y     Trim in the y-axis
 *
 */
void DualAxisAnalogSensor::setTrim(float x, float y)
{
	// Save trim values
	this->_trim.X = x;
	this->_trim.Y = y;
}

/**
 * Sets the gain values for the sensor. Update() has to be called
 * before these gain values are applied to the output value.
 *
 * @param x     Gain in the x-axis
 * @param y     Gain in the y-axis
 *
 */
void DualAxisAnalogSensor::setGain(float x, float y)
{
	// Save trim values
	this->_gain.X = x;
	this->_gain.Y = y;
}

