#ifndef VECTOR_H
#define VECTOR_H

//
// Vector data
//
struct Vector
{
    float X;
    float Y;
    float Z;
};

#endif
