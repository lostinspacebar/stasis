/**
 * PID Controller class
 * ----------------------------------------
 * 
 */

#ifndef PID_H
#define PID_H

struct PIDTuning
{
	float P;
	float I;
	float D;
	float SetPoint;
};

class PID
{
public:
	
	/**
	 *  Default Constructor
	 */
	PID();
	
	/**
	 * Constructor
	 */
	PID(float p, float i, float d, float setPoint);
	
	/**
	 * Update the PID controller with a new input value
	 * 
	 * @param newValue	New process variable value
	 * @return			Output of the PID controller
	 */
	float update(float newValue);
	
	/**
	 * Gets the output of the PID controller
	 */
	float getOutput()
	{
		return _output;
	}
	
	/**
	 * Gets the current error of the PID controller
	 */
	float getError()
	{
		return _currentError;
	}
	
	void reset()
	{
		_pError = 0;
		_iError = 0;
		_dError = 0;
		_accumulatedError = 0;
		_currentError = 0;
		_integrationWindowStartCursor = 0;
		_integrationWindowCount = 0;
	}
	
	void tune(PIDTuning tuning)
	{
		P = tuning.P;
		I = tuning.I;
		D = tuning.D;
		SetPoint = tuning.SetPoint;
		
		reset();
	}
	
	float P;
	float I;
	float D;
	
	float SetPoint;
	
private:
	
	float _pError;
	float _iError;
	float _dError;
	
	float _accumulatedError;
	
	float _output;
	float _currentError;
	
	float _integrationWindow[10];
	int _integrationWindowStartCursor;
	int _integrationWindowWriteCursor;
	int _integrationWindowCount;
	int _integrationWindowSize;
};

#endif
