/**
 * Statis Robot Controller
 * -------------------------------------------------------------
 * Main controller for the stasis robot logic
 *
 */

#ifndef STASISCONTROLLER_H
#define STASISCONTROLLER_H

// Includes
#include "wirish.h"
#include "PID.h"
#include "LQR.h"
#include "Message.h"

class StasisController
{

public:

	/**
	 * Initialize the controller instance
	 *
	 * @param robot    Robot that this controller will be err... controlling.
	 *
	 */
	void initialize(StasisRobot *robot);

	/**
	 * Let's the controller perform a think iteration.
	 *
	 */
	void think();
	
	/**
	 * Handle tuning message from PC
	 */
	void handlePIDMessage(PIDTuningMessage *message);
	
	/**
	 * Handle LQR tuning message from the PC
	 */
	void handleLQRMessage(LQRTuningMessage *message);
	
	/**
	 * Gets the average FPS of the controller
	 */
	uint32 getFps()
	{
		return this->_fps;
	};

private:
	
	/**
	 * Calculates the average "frames"/iterations per second
	 */
	void calculateFps();

	// Robot instance we will be controlling
	StasisRobot *_robot;
	
	// FPS calculation state
	uint32 _fps;
	uint32 _fpsTemp;
	uint32 _lastFpsTimer;
	
	// PID
	PID _displacementPID;
	PID _velocityPID;
	PID _anglePID;
	PID _angularVelocityPID;
	
	// LQR
	LQR _lqr;
	Matrix _xMatrix;
	
	// Loop Time
	uint32 _lastCall;
	uint32 _loopTime;
	
	// Last state
	float _lastTiltX;

};

#endif

