/**
 * Analog Gyro
 * -------------------------------------------------------------
 * Class that interfaces with an analog gyro. This 
 * monitors 3 analog inputs.
 *
 */

#ifndef ANALOGGYRO_H
#define ANALOGGYRO_H

// Includes
#include "wirish.h"
#include "Vector.h"
#include "DualAxisAnalogSensor.h"

class AnalogGyro : public DualAxisAnalogSensor
{

};

#endif

