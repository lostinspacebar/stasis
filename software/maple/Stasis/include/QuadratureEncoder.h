/**
 * Quadrature Encoder
 * -------------------------------------------------------------
 * Class for interfacing with quarature encoders with two 
 * pulse outputs
 *
 */

#ifndef QUADRATURE_ENCODER_H
#define QUADRATURE_ENCODER_H

// Includes
#include "wirish.h"

class QuadratureEncoder
{

public:

	/**
	 * Intializes the quadrature encoder instance
	 *
	 * @param outputAPin    		Output pin for Hall Sensor A
	 * @param outputBPin    		Output pin for Hall Sensor B
	 * @param countsPerRevolution	Counts per revolution of the output shaft 
	 * 								(after the gear box)
	 * @param wheelDiameter			Wheel diameter in millimeters
	 *
	 */
	void initialize(uint8 outputAPin, uint8 outputBPin, float countsPerRevolution, uint16 wheelDiameter);

	/**
	 * Updates the encoder count since the last time Update() was called
	 *
	 */
	void update();

	/**
	 * Returns the encoder count since the last call to Update().
	 *
	 * @return Encoder count
	 *
	 */
	long getValue();
	
	/**
	 * Calculates and returns the displacement since the last call 
	 * to update using the specified wheel diameter.
	 * 							
	 * @return	Return the displacement in millimeters
	 */
	float getDisplacement();

private:

	/**
	 * Interrupt handler for the hall effect output on the encoder
	 *
	 */
	static void EncoderInterruptHandler(void *instance);

	// Output pins
	uint8 _outputAPin;
	uint8 _outputBPin;

	// Encoder count since last call to Update
	long _encoderCount;
	
	// Counts per revolution at the output of the gearbox
	float _countsPerRevolution;
	
	// Wheel diameter in millimeters
	uint16 _wheelCircumference;

	// Running encoder count that's updated in the interrupt
	long _runningEncoderCount;

};

#endif
