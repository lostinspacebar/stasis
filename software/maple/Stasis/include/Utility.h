/**
 * Utility
 * -------------------------------------------------------------
 * Provides some useful utility functions
 *
 */

#ifndef UTILITY_H
#define UTILITY_H

// Includes
#include "wirish.h"
#include <string.h>

class Utility
{
public:
	
	/**
	 * Converts byte data to a 16-bit unsigned int.
	 * 
	 * @param buffer	Byte buffer (of length 2)
	 * @return			16-bit unsigned integer value
	 */
	static uint16 convertToUInt16(uint8 *buffer)
	{
		uint16 val;
		memcpy(&val, buffer, 2);
		
		return val;
	};
	
	/**
	 * Converts byte data to a 32-bit unsigned int.
	 * 
	 * @param buffer	Byte buffer (of length 4)
	 * @return			32-bit unsigned integer value
	 */
	static uint32 convertToUInt32(uint8 *buffer)
	{
		uint32 val;
		memcpy(&val, buffer, 4);
		
		return val;
	};
	
	/**
	 * Converts byte data to a 16-bit signed int.
	 * 
	 * @param buffer	Byte buffer (of length 2)
	 * @return			16-bit signed integer value
	 */
	static int16 convertToInt16(uint8 *buffer)
	{
		int16 val;
		memcpy(&val, buffer, 2);
		
		return val;
	};
	
	/**
	 * Converts byte data to a 32-bit signed int.
	 * 
	 * @param buffer	Byte buffer (of length 4)
	 * @return			32-bit signed integer value
	 */
	static int32 convertToInt32(uint8 *buffer)
	{
		int32 val;
		memcpy(&val, buffer, 4);
		
		return val;
	};
	
	/**
	 * Converts byte data to a 32-bit float
	 * 
	 * @param buffer	Byte buffer (of length 4)
	 * @return			32-bit float value
	 */
	static float convertToFloat(uint8 *buffer)
	{
		return ((float)convertToInt32(buffer)) / 10000.0f;
	};
	
	/**
	 *  Converts a float value to an array of bytes. The data 
	 *  is stored in the "buffer" variable passed in.
	 *  
	 *  @param val		Value to convert
	 *  @param buffer	Array to store converted float value in
	 */
	static void convertToByteArray(float val, uint8 *buffer)
	{
		val = val * 10000.0f;
		int intVal = (int)val;
		buffer[0] = intVal & 0xFF;
		buffer[1] = (intVal >> 8) & 0xFF;
		buffer[2] = (intVal >> 16) & 0xFF;
		buffer[3] = (intVal >> 24) & 0xFF;
	};
	
};

#endif
