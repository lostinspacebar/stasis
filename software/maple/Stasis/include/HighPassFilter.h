/*
 * HighPassFilter.h
 *
 *  Created on: Sep 20, 2012
 *      Author: aditya
 */

#ifndef HIGHPASSFILTER_H_
#define HIGHPASSFILTER_H_

class HighPassFilter
{
public:
	
	HighPassFilter(int order)
	{
		_output = 0;
		_order = order;
		_dataSize = _order;
		_dataCursor = 0;
	}
	
	float update(float newValue);
	
private:
	
	float _output;
	float _data[10];
	int _dataSize;
	int _dataCount;
	int _dataCursor;
	int _order;
	
};


#endif /* HIGHPASSFILTER_H_ */
