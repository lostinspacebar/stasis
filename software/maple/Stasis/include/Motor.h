/**
 * Motor
 * -------------------------------------------------------------
 * Class that interfaces with motor drivers that are similar to 
 * the VNH3SP30 from Pololu
 *
 */

#ifndef MOTOR_H
#define MOTOR_H

// Includes
#include "wirish.h"

class Motor
{
public:

	/**
	 * Initializes the Motor instance
	 *
	 * @param pwmPin               Motor PWM control pin
	 * @param directionAPin        Direction Control Pin A
	 * @param directionBPin        Direction Control Pin B
	 * @param isReversed		   Is the motor reversed the way it's mounted?
	 *
	 */
	void initialize(uint8 pwmPin, uint8 directionAPin, uint8 directionBPin, bool isReversed);

	/**
	 * Sets the throttle position for motor control. This is a value between -1.0 and 1.0.
	 *
	 * @param throttle             Throttle value.
	 *                             -1.0 is full reverse throttle
	 *                             0.0 is a complete stop
	 *                             1.0 is full forward throttle.
	 *
	 */
	void setThrottle(float throttle);

	/**
	 * Gets the currently set throttle position
	 *
	 */
	float getThrottle()
	{
		return this->_throttle;
	}

private:

	// Current throttle position
	float _throttle;

	// Pins
	uint8 _pwmPin;
	uint8 _dirAPin;
	uint8 _dirBPin;
	
	// Reversed?
	bool _isReversed;

};

#endif
