/*
 * AveragingFilter.h
 *
 *  Created on: Sep 19, 2012
 *      Author: aditya
 */

#ifndef AVERAGINGFILTER_H_
#define AVERAGINGFILTER_H_

class AveragingFilter
{
public:
	
	AveragingFilter(int size)
	{
		_windowSize = size;
		_windowCount = 0;
		_cursor = 0;
	}
	
	virtual float update(float newValue)
	{
		_window[_cursor++] = newValue;
		_windowCount++;
		if(_cursor >= _windowSize)
		{
			_cursor = 0;
		}
		if(_windowCount > _windowSize)
		{
			_windowCount = _windowSize;
		}
		
		_output = 0;
		for(int i = 0; i < _windowCount; i++)
		{
			_output += _window[i];
		}
		_output = _output / _windowCount;
		
		return _output;
	}

private:
	
	float _window[20];
	int _windowSize;
	int _windowCount;
	int _cursor;
	float _output;
	
};

#endif /* AVERAGINGFILTER_H_ */
