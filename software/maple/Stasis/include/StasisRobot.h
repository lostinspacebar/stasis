/**
 * Statis Robot
 * -------------------------------------------------------------
 * Allows interfacing with hardware on the Robot. No real 
 * control logic here.
 *
 */

#ifndef STASISROBOT_H
#define STASISROBOT_H

// Includes
#include "wirish.h"
#include "AnalogAccelerometer.h"
#include "AnalogGyro.h"
#include "Motor.h"
#include "QuadratureEncoder.h"

class StasisRobot
{
public:

	/**
	 * Initializes the stasis robot instance
	 *
	 * @param accel				Accelerometer instance
	 * @param gyro				Gyro instance
	 * @param leftEncoder		Left motor encoder instance
	 * @param rightEncoder		Right motor encoder instance
	 * @param leftMotor			Left motor instance
	 * @param rightMotor		Right motor instance
	 *
	 */
	void initialize(AnalogAccelerometer *accel, AnalogGyro *gyro,
			 	 	QuadratureEncoder *leftEncoder, QuadratureEncoder *rightEncoder,
			 	 	Motor *leftMotor, Motor *rightMotor);

	/**
	 * Update sensors and related state on the robot
	 *
	 */
	void update(uint32 loopTime);
	
	/**
	 * Resets state and stores new accelerometer starting tilt point
	 */
	void reset();

	/**
	 * Gets the acceleration of the robot since the last 
	 * call to UpdateSensors
	 *
	 */
	Vector getAcceleration()
	{
		return this->_accel->getValue();
	}

	/**
	 * Gets the angular rate of the robot in deg/sec
	 * since last call to UpdateSensors
	 *
	 */
	Vector getAngularRate()
	{
		return this->_gyro->getValue();
	}
	
	/**
	 * Gets the calculated tilt angle in x,y,z
	 */
	Vector getTilt()
	{
		return this->_tilt;
	}
	
	/**
	 * Gets the calculated displacement in x,y,z
	 */
	Vector getDisplacement()
	{
		return this->_displacement;
	}
	
	/**
	 * Sets throttle values for left and right motors on the robot
	 */
	void setThrottle(double left, double right)
	{
		this->_leftMotor->setThrottle(left);
		this->_rightMotor->setThrottle(left);
	}

private:

	/**
	 * Calculates tilt around a single axis given acceleration vector components
	 * 
	 * @param x		Acceleration component in the "x" direction
	 * @param y 	Acceleration component in the "y" direction
	 * 
	 * @return		Tilt in a single axis in degrees
	 * 
	 */
	float calculateAxisTilt(float x, float y);
	
	// Sensor instances
	AnalogAccelerometer *_accel;
	AnalogGyro *_gyro;
	QuadratureEncoder *_leftEncoder;
	QuadratureEncoder *_rightEncoder;
	Motor *_leftMotor;
	Motor *_rightMotor;
	
	// Calculated tilt angle (in degress) of the robot.
	// 90 is vertical. 0 is "on it's face". 180 is "on it's back"
	Vector _tilt;

	// Calculated displacement
	Vector _displacement;
};

#endif

