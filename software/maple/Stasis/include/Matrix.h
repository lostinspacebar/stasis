/**
 * Matrix 
 * -------------------------------------------------------------
 * Simple matrix class.
 *
 */

#ifndef MATRIX_H
#define MATRIX_H

#include <wirish.h>
#include <string.h>

class Matrix
{
public:
	
	Matrix()
	{
		_rows = 4;
		_cols = 4;
	}
	
	float operator() (int row, int col)
	{
		return _data[row][col];
	}
	
	void copy(Matrix x)
	{
		for(int i = 0; i < _rows; i++)
		{
			for(int p = 0; p < _cols; p++)
			{
				_data[i][p] = x(i, p);
			}
		}
	}
	
	void setValue(int row, int col, float value)
	{
		_data[row][col] = value;
	}
	
	void print()
	{
		for(int i = 0; i < _rows; i++)
		{
			for(int p = 0; p < _cols; p++)
			{
				SerialUSB.print(_data[i][p]);
				SerialUSB.print(", ");
			}
			SerialUSB.print("\n");
		}
	}
	
	void setSize(int rows, int cols)
	{
		_rows = rows;
		_cols = cols;
	}
	
private:
	
	int _rows;
	int _cols;
	float _data[4][4];
};

#endif
