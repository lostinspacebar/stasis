/*
 * MedianFilter.h
 *
 *  Created on: Sep 19, 2012
 *      Author: aditya
 */

#ifndef MEDIANFILTER_H_
#define MEDIANFILTER_H_

class MedianFilter
{
public:
	
	MedianFilter(int size)
	{
		_dataSize = size;
		_dataCount = 0;
		_dataCursor = 0;
	}
	
	virtual float update(float newValue)
	{
		// Add to data
		_data[_dataCursor++] = newValue;
		_dataCount++;
		
		// Data count is never higher than size
		if(_dataCount > _dataSize)
		{
			_dataCount = _dataSize;
		}
		
		// Make sure cursor stays valid
		if(_dataCursor > _dataSize)
		{
			_dataCursor = 0;
		}
		
		// Return value passed in by default
		_output = newValue;
		
		// If the median buffer is full, we can start
		// calculating median values.
		if(_dataCount == _dataSize)
		{
			// Insertion sort into _sorted
			for(int i = 0; i < _dataCount; i++)
			{
				// Move to end of array
				_sorted[i] = _data[i];
				
				// Swap backwards until in order
				for(int p = i; p > 0; p--)
				{
					float a = _sorted[p];
					float b = _sorted[p - 1];
					if(b > a)
					{
						_sorted[p - 1] = a;
						_sorted[p] = b;
					}
				}
			}
			
			// Get median
			_output = _sorted[(int)(_dataCount / 2)];
		}
		
		// Return new output
		return _output;
	}

private:
	
	float _data[10];
	int _dataSize;
	int _dataCount;
	int _dataCursor;
	
	float _sorted[10];
	
	float _output;
	
};

#endif
