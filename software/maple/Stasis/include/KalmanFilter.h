/*
 * KalmanFilter.h
 *
 *  Created on: Sep 20, 2012
 *      Author: aditya
 */

#ifndef KALMANFILTER_H_
#define KALMANFILTER_H_

#include <wirish.h>

class KalmanFilter
{
public:
	
	/**
	 * Constructor
	 */
	KalmanFilter(float qAngle, float qGyro, float rAngle);
	
	/**
	 * Calculates proper tilt given Accelerometer and Gyro readings.
	 * 
	 * @param newAngle		Angle calculated from accelerometer
	 * @param newRate		Newest gyro readin
	 * @param loopTime		Loop time in milliseconds
	 * 
	 * @return	Gyro compensated tilt value
	 */
	float update(float newAngle, float newRate, int loopTime);
	
private:
	
	float _qAngle;
	float _qGyro;
	float _rAngle;
	float _xAngle;
	float _xBias;
	float _P_00, _P_01, _P_10, _P_11;
	float _output;
};

#endif /* KALMANFILTER_H_ */
