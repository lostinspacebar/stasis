/**
 * Message
 * -------------------------------------------------------------
 * Simple message class.
 *
 */

#ifndef MESSAGE_H
#define MESSAGE_H

#include <wirish.h>
#include <string.h>
#include <Matrix.h>
#include <Utility.h>
#include <PID.h>

/**
 * Types of messages that be sent/received
 */
enum MessageType
{
	DEBUG = 0x55,
	LQR_TUNING = 0x44,
	PID_TUNING = 0x45,
	REPORT_VALUES = 0x46,
};

class Message
{
public:
	MessageType getType()
	{
		return _type;
	}
	
	uint8 *getData()
	{
		return _data;
	}
	
	uint8 getLength()
	{
		return _dataLength;
	}
	
protected:
	
	MessageType _type;
	uint8 _data[256];
	uint16  _dataLength;
};

class ReportValuesMessage : public Message
{
public:
	ReportValuesMessage(float *values, int numValues);
};

class DebugMessage : public Message
{
public:
	DebugMessage(const char *message);
};

class LQRTuningMessage : public Message
{
public:
	LQRTuningMessage(uint8 *data, uint16 length);
	
	Matrix getAMatrix()
	{
		return _aMatrix;
	}
	
	Matrix getQMatrix()
	{
		return _qMatrix;
	}
	
	Matrix getKMatrix()
	{
		return _kMatrix;
	}
	
private:
	
	Matrix _aMatrix;
	Matrix _qMatrix;
	Matrix _kMatrix;
};

class PIDTuningMessage : public Message
{
public:
	PIDTuningMessage(uint8 *data, uint16 length);
	
	PIDTuning getDisplacementTuning()
	{
		return _x;
	};
	
	PIDTuning getVelocityTuning()
	{
		return _dX;
	};
	
	PIDTuning getAngleTuning()
	{
		return _a;
	};
	
	PIDTuning getAngularVelocityTuning()
	{
		return _dA;
	};
	
private:
	
	PIDTuning _x;
	PIDTuning _dX;
	PIDTuning _a;
	PIDTuning _dA;
};

#endif
