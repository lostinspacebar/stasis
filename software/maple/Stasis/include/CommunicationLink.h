/**
 * Communication Link
 * -------------------------------------------------------------
 * Provides functionality to communicate with the command center
 * application running on a PC over WiFi or USB-Serial
 *
 */

#ifndef COMMUNICATIONLINK_H
#define COMMUNICATIONLINK_H

// Includes
#include "wirish.h"
#include "Message.h"

/**
 * Event handler signature for command center messages from the PC
 */
typedef void (*MessageHandler)(Message *);

/**
 * Communication link
 */
class CommunicationLink
{
public:
	
	/**
	 * Initializes the communication link
	 * 
	 * @param wifiSerialPort	Serial port to use to communicate over the WiFi module
	 * @param useUSBIfAvailable Determines whether the USB serial port is used if connected.
	 * @param rxHandler			Handler for messages received from the command center
	 * 
	 */
	void initialize(HardwareSerial *wifiSerialPort, bool useUSBIfAvailable, MessageHandler rxHandler);
	
	/**
	 * Read in any available data on the Serial Port
	 * 
	 */
	void read();
	
	/**
	 *  Writes the specified message to the link
	 */
	static void writeMessage(Message message);
	
	/**
	 *  Gets the singleton instance
	 */
	static CommunicationLink *instance()
	{
		if(_instance == NULL)
		{
			_instance = new CommunicationLink();
		}
		return _instance;
	}

private:
	
	static CommunicationLink * _instance;
	
	// Serial port used for WiFi communication (using the WiFi module)
	HardwareSerial *			_port;
	
	// Data RX Handler
	MessageHandler 				_rxHandler;
	
	// Running message buffer
	uint8						_buffer[256];
	
	// Running message buffer length
	uint16						_bufferLength;
	
};

#endif
