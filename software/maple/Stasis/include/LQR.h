/*
 * LQR.h
 *
 *  Created on: Sep 20, 2012
 *      Author: aditya
 */

#ifndef LQR_H_
#define LQR_H_

#include "Matrix.h"

class LQR
{
public:
	
	/**
	 * Constructor
	 */
	LQR();
	
	/**
	 * Update LQR with new value
	 * 
	 * @param 	x	New 'x' matrix. i.e. Observable values
	 * @return 	New LQR Output
	 */
	float update(Matrix x);
	
	/**
	 * Set the K matrix used for this LQR
	 * 
	 * @param k		New K matrix
	 */
	void setKMatrix(Matrix k)
	{
		_kMatrix.copy(k);
		
		// Reset output
		_output = 0;
	}
	
private:
	
	// Output of the LQR
	float _output;
	
	// K-Matrix
	Matrix _kMatrix;
};

#endif /* LQR_H_ */
