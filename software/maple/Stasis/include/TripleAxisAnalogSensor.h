/**
 * Triple Axis Analog Sensor
 * -------------------------------------------------------------
 * Class that interfaces with analog sensors that measure stuff
 * in 3 axes, where each analog output has an associated analog 
 * pin.
 *
 */

#ifndef TRIPLE_AXIS_ANALOG_SENSOR_H
#define TRIPLE_AXIS_ANALOG_SENSOR_H

// Includes
#include "wirish.h"
#include "Vector.h"

class TripleAxisAnalogSensor
{

public:

	/**
	 * Initializes the analog sensor instance
	 *
	 * @param xpin            Analog X input
	 * @param ypin            Analog Y input
	 * @param zpin            Analog Z input
	 * @param centerVoltage   Voltage that the sensor puts out a zero-value in millivolts
	 * @param sensitivity     Sensitivity of the sensor in mV/sensor-unit
	 *
	 */
	void initialize(uint8 xpin, uint8 ypin, uint8 zpin, float centerVoltage, float sensitivity);

	/**
	 * Updates values from all analog pins
	 *
	 */
	void update();

	/**
	 * Gets the last updated value from the sensor
	 *
	 * @return Sensor value as a vector
	 *
	 */
	Vector getValue()
	{
		return this->_value;
	}

	/**
	 * Gets the current trim for the sensor
	 *
	 * @return Trim values as a vector
	 *
	 */
	Vector getTrim()
	{
		return this->_trim;
	}

	/**
	 * Sets the trim values for the sensor. Update() has to be called
	 * before these trim values are applied to the output value.
	 *
	 * @param x     Trim in the x-axis
	 * @param y     Trim in the y-axis
	 * @param z     Trim in the z-axis
	 *
	 */
	void setTrim(float x, float y, float z);
	
	/**
	 * Gets the current gain for the sensor
	 *
	 * @return Gain values as a vector
	 *
	 */
	Vector getGain()
	{
		return this->_gain;
	}

	/**
	 * Sets the gain values for the sensor. Update() has to be called
	 * before these gain values are applied to the output value.
	 *
	 * @param x     Gain in the x-axis
	 * @param y     Gain in the y-axis
	 * @param z     Gain in the z-axis
	 *
	 */
	void setGain(float x, float y, float z);

private:

	// Pins
	uint8 _xpin;
	uint8 _ypin;
	uint8 _zpin;

	// Sensor trim
	Vector _trim;
	
	// Sensor gain
	Vector _gain;

	// Sensor range description
	float _centerVoltage;
	float _sensitivity;

	// Value vector
	Vector _value;

};

#endif

