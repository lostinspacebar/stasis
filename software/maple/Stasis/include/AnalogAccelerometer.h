/**
 * Analog Accelerometer
 * -------------------------------------------------------------
 * Class that interfaces with an analog accelerometer. This 
 * monitors 3 analog inputs.
 *
 */

#ifndef ANALOGACCEL_H
#define ANALOGACCEL_H

// Includes
#include "wirish.h"
#include "Vector.h"
#include "TripleAxisAnalogSensor.h"

//
// Accelerometer class
//
class AnalogAccelerometer : public TripleAxisAnalogSensor
{

};

#endif

