/**
 * Motor
 * -------------------------------------------------------------
 * Class that interfaces with motor drivers that are similar to 
 * the VNH3SP30 from Pololu
 *
 */

// Includes
#include "Motor.h"
#include <stdio.h>
#include <string.h>
#include "WProgram.h"
#include <math.h>

/**
 * Initializes the Motor instance
 *
 * @param pwmPin               Motor PWM control pin
 * @param directionAPin        Direction Control Pin A
 * @param directionBPin        Direction Control Pin B
 * @param isReversed		   Is the motor reversed the way it's mounted?
 *
 */
void Motor::initialize(uint8 pwmPin, uint8 directionAPin, uint8 directionBPin, bool isReversed)
{
	// Save pins for later
	this->_pwmPin = pwmPin;
	this->_dirAPin = directionAPin;
	this->_dirBPin = directionBPin;

	// Initialize pin modes
	pinMode(this->_pwmPin, PWM);
	pinMode(this->_dirAPin, OUTPUT);
	pinMode(this->_dirBPin, OUTPUT);
	
	// Is the motor going to be reversed?
	this->_isReversed = isReversed;

	// Start at 0 throttle
	this->setThrottle(0);
}

/**
 * Sets the throttle position for motor control. This is a value between -1.0 and 1.0.
 *
 * @param throttle             Throttle value. 
 *                             -1.0 is full reverse throttle
 *                             0.0 is a complete stop
 *                             1.0 is full forward throttle.
 *
 */
void Motor::setThrottle(float throttle)
{
	// Limit throttle value
	if(throttle > 1.0)
	{
		throttle = 1.0;
	}
	if(throttle < -1.0)
	{
		throttle = -1.0;
	}
	
	// Save throttle value
	this->_throttle = throttle;
	
	// Set PWM
	pwmWrite(this->_pwmPin, (uint16)floor(abs(throttle) * 65535.0));
	
	// When we are actually setting the value to the controller,
	// we should flip it if the motor is indicated as being reversed.
	// This happens if the motor is mounted in a way that 
	// "forward" throttle actually makes the mounted wheel go in 
	// "reversed" with respect to the robot
	if(this->_isReversed)
	{
		throttle *= -1.0;
	}
	
	// Set direction
	if (throttle == 0.0)
	{
		digitalWrite(this->_dirAPin, LOW);
		digitalWrite(this->_dirBPin, LOW);
	}
	else if (throttle > 0.0)
	{
		digitalWrite(this->_dirAPin, HIGH);
		digitalWrite(this->_dirBPin, LOW);
	}
	else if (throttle < 0.0)
	{
		digitalWrite(this->_dirAPin, LOW);
		digitalWrite(this->_dirBPin, HIGH);
	}
}
