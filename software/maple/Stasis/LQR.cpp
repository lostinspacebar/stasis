/*
 * LQR.cpp
 *
 *  Created on: Sep 20, 2012
 *      Author: aditya
 */

#include "LQR.h"

LQR::LQR()
{
	_output = 0;
}

float LQR::update(Matrix x)
{
	for(int i = 0; i < 4; i++)
	{
		_output -= _kMatrix(0, i) * x(i, 0);
	}
	
	return _output;
}
