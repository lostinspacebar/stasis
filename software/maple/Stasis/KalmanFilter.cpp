/*
 * KalmanFilter.cpp
 *
 *  Created on: Sep 20, 2012
 *      Author: aditya
 */

#include "KalmanFilter.h"

KalmanFilter::KalmanFilter(float qAngle, float qGyro, float rAngle)
{
	_qAngle = qAngle;
	_qGyro = qGyro;
	_rAngle = rAngle;
	_xAngle = _xBias = _P_00 = _P_01 = _P_10 = _P_11 = 0.0;
}

float KalmanFilter::update(float newAngle, float newRate, int loopTime)
{
	// Time delta
	float dt = (float)loopTime / 1000.0;
	
	_xAngle += dt * (newRate - _xBias);
	_P_00 +=  -dt * (_P_10 + _P_01) + _qAngle * dt;
	_P_01 +=  -dt * _P_11;
	_P_10 +=  -dt * _P_11;
	_P_11 +=  +_qGyro * dt;
	
	float y = newAngle - _xAngle;
	float S = _P_00 + _rAngle;
	float K_0 = _P_00 / S;
	float K_1 = _P_10 / S;
	
	_xAngle +=  K_0 * y;
	_xBias  +=  K_1 * y;
	_P_00 -= K_0 * _P_00;
	_P_01 -= K_0 * _P_01;
	_P_10 -= K_1 * _P_00;
	_P_11 -= K_1 * _P_01;
	
	return _xAngle;
}
