#include "Message.h"

ReportValuesMessage::ReportValuesMessage(float *values, int numValues)
{
	// Set type
	_type = REPORT_VALUES;
	
	// Length
	_dataLength = (numValues * 4) + 2;
	
	_data[0] = numValues & 0xFF;
	_data[1] = (numValues >> 8) & 0xFF;
	
	// Save values into byte array
	int count = 2;
	for(int i = 0; i < numValues; i++)
	{
		Utility::convertToByteArray(values[i], _data + count);
		count += 4;
	}
}

DebugMessage::DebugMessage(const char *message)
{
	// Set type
	_type = DEBUG;
	
	// Set length
	_dataLength = strlen(message);
	
	// Set data
	memcpy(_data, message, _dataLength);
}

LQRTuningMessage::LQRTuningMessage(uint8 *data, uint16 length)
{
	// Set type
	_type = LQR_TUNING;
	
	// Set length
	_dataLength = length;
	
	// Fill 'A' Matrix
	int count = 1;
	for(int i = 0; i < 4; i++)
	{
		for(int p = 0; p < 4; p++)
		{
			float val = Utility::convertToFloat(data + count);
			_aMatrix.setValue(i, p, val);
			count += 4;
		}
	}
	
	// Fill Q Matrix
	for(int i = 0; i < 4; i++)
	{
		for(int p = 0; p < 4; p++)
		{
			float val = Utility::convertToFloat(data + count);
			_qMatrix.setValue(i, p, val);
			count += 4;
		}
	}
	
	// Fill K matrix
	for(int i = 0; i < 4; i++)
	{
		for(int p = 0; p < 4; p++)
		{
			float val = Utility::convertToFloat(data + count);
			_kMatrix.setValue(i, p, val);
			count += 4;
		}
	}
}

PIDTuningMessage::PIDTuningMessage(uint8 *data, uint16 length)
{
	// Set type
	_type = PID_TUNING;
	
	// Set length
	_dataLength = length;
	
	// Save tuning
	int count = 1;
	_x.P = Utility::convertToFloat(data + count);
	count += 4;
	_x.I = Utility::convertToFloat(data + count);
	count += 4;
	_x.D = Utility::convertToFloat(data + count);
	count += 4;
	_x.SetPoint = Utility::convertToFloat(data + count);
	count += 4;
	
	_dX.P = Utility::convertToFloat(data + count);
	count += 4;
	_dX.I = Utility::convertToFloat(data + count);
	count += 4;
	_dX.D = Utility::convertToFloat(data + count);
	count += 4;
	_dX.SetPoint = Utility::convertToFloat(data + count);
	count += 4;
	
	_a.P = Utility::convertToFloat(data + count);
	count += 4;
	_a.I = Utility::convertToFloat(data + count);
	count += 4;
	_a.D = Utility::convertToFloat(data + count);
	count += 4;
	_a.SetPoint = Utility::convertToFloat(data + count);
	count += 4;
	
	_dA.P = Utility::convertToFloat(data + count);
	count += 4;
	_dA.I = Utility::convertToFloat(data + count);
	count += 4;
	_dA.D = Utility::convertToFloat(data + count);
	count += 4;
	_dA.SetPoint = Utility::convertToFloat(data + count);
	count += 4;
}
